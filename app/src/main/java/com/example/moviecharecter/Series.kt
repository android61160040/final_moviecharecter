package com.example.moviecharecter

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Series : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_series)

        var Btn_Home : Button? = null
        Btn_Home = findViewById<Button>(R.id.btn_home)
        Btn_Home!!.setOnClickListener{
            var intent= Intent(this,MainActivity::class.java)
            startActivity(intent)
        }
        var Btn_movie : Button? = null
        Btn_movie = findViewById<Button>(R.id.btn_movie)
        Btn_movie!!.setOnClickListener{
            var intent= Intent(this,Movie::class.java)
            startActivity(intent)
        }
    }
}