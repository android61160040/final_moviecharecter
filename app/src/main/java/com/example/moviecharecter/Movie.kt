package com.example.moviecharecter

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Movie : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)

        var Btn_Home : Button? = null
        Btn_Home = findViewById<Button>(R.id.btn_home)
        Btn_Home!!.setOnClickListener{
            var intent= Intent(this,MainActivity::class.java)
            startActivity(intent)
        }
        var Btn_series : Button? = null
        Btn_series = findViewById<Button>(R.id.btn_series)
        Btn_series!!.setOnClickListener{
            var intent= Intent(this,Series::class.java)
            startActivity(intent)
        }
    }
}