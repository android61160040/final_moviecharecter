package com.example.moviecharecter

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var Btn_series : Button? = null
        Btn_series = findViewById<Button>(R.id.btn_series)
        Btn_series!!.setOnClickListener{
            var intent= Intent(this,Series::class.java)
            startActivity(intent)
        }
        var Btn_movie : Button? = null
        Btn_movie = findViewById<Button>(R.id.btn_movie)
        Btn_movie!!.setOnClickListener{
            var intent= Intent(this,Movie::class.java)
            startActivity(intent)
        }
    }
}